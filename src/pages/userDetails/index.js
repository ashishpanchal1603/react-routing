import React, { useState, useEffect } from 'react'
import instance from '../../shared/constant'
import './style.css'
import { useNavigate } from 'react-router-dom'
function UserDetails () {
  const [searchItem, setSearchItem] = useState('')
  const [inputText, setInputText] = useState('')
  const [data, setData] = useState([])
  const [order, setOrder] = useState('ASC')
  const navigate = useNavigate()

  // sorting method

  const dataSorting = (col) => {
    if (order === 'ASC') {
      const sorted = [...ReData].sort((a, b) =>
        a[col].toLowerCase() > b[col].toLowerCase() ? 1 : -1
      )
      setData(sorted)
      setOrder('ASC')
    }
  }
  const getData = () => {
    instance.get('data').then((res) => setData(res.data))
  }
  useEffect(() => {
    getData()
  }, [])

  const ReData = data.filter((val) => {
    if (searchItem === '') {
      return val
    } else {
      return val.firstName.toLowerCase().includes(searchItem.toLowerCase())
    }
  })

  // delete
  const handleDelete = (e, id) => {
    instance
      .delete(`data/${id}`)
      .then((res) => {
        console.log('Deleted Data :>> ', res.data)
        alert('Data Deleted Successfully...')
      })
      .then(() => {
        getData()
      })
      .catch((error) => {
        console.log('error :>> ', error)
      })
    e.preventDefault()
  }

  // Find All method
  const findMale = () => {
    const filterData = [...ReData].filter((item) => item.gender === 'Male')
    setData(filterData)
  }
  const findFemale = () => {
    const filterData = [...ReData].filter((item) => item.gender === 'Male')
    setData(filterData)
  }
  const getAllData = () => {
    getData()
  }

  // change
  const handleClick = (e, item) => {
    instance
      .put(`data/${item.id}`, {
        ...item,
        status: !item.status
      })
      .then((res) => console.log('res.data', res.data))
      .then(() => {
        getData()
      })
    e.preventDefault()
  }

  // edit
  const handleEdit = (e, item) => {
    console.log('data', item)
    navigate('/updatePage', { state: { item } })
  }

  // searchBar
  const showEnterData = (e) => {
    setSearchItem('')
    setInputText(e.target.value)
  }
  const delayData = (e) => {
    setSearchItem(inputText)
  }

  const displayData = ReData.map((item, index) => {
    return (
      <tr key={index}>
        <td> {item.firstName}</td>
        <td> {item.lastName}</td>
        <td> {item.email}</td>
        <td onClick={(e) => handleClick(e, item)}>
          {item.status === true ? 'true' : 'false'}
        </td>
        <td>
          <button onClick={(e) => handleEdit(e, item)}>Edit</button>
          <button onClick={(e) => handleDelete(e, item?.id)}> Delete</button>
        </td>
      </tr>
    )
  })
  return (
    <>
      <div className="main">
        <div className="userTable">
          <input
            type="text"
            placeholder="search.....Enter"
            onChange={(e) => {
              showEnterData(e)
            }}
          />
          <button className="btn btn-warning" onClick={() => delayData()}>
            Search
          </button>

          <button onClick={() => findMale()}>Male </button>
          <button onClick={() => findFemale()}>Female </button>
          <button onClick={() => getAllData()}>All Data </button>
          <button onClick={() => navigate('/addUser')}>AddUser</button>
          <table>
            <thead>
              <tr>
                <td onClick={() => dataSorting('firstName')}> firstName</td>
                <td> lastName</td>
                <td> email</td>
                <td> status</td>
                <td colSpan={2}> Action</td>
              </tr>
            </thead>
            <tbody>{displayData}</tbody>
          </table>
        </div>
      </div>
    </>
  )
}

export default UserDetails
