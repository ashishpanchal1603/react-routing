import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Navbar from '../../shared/components/navbar'
import Login from '../login'
import SingUp from '../singUp'
import DashBoard from '../../shared/components/dashboard'
import UserDetails from '../userDetails'
import UpdatePage from '../update'
import Protected from '../../shared/components/protected'
function Routing () {
  return (
    <>
      <Router>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/singUp" element={<SingUp />} />
          <Route path="/navbar" element={<Protected Component={Navbar}/> }>
            <Route index element={<Protected Component={DashBoard} />} />
            <Route path="user" element={<Protected Component={UserDetails} />} />
          </Route>
          <Route path="/addUser" element={ <Protected Component={SingUp}/>} />
          <Route path="/updatePage" element={<Protected Component={UpdatePage}/> } />
        </Routes>
      </Router>
    </>
  )
}

export default Routing
