import React, { useEffect, useState } from 'react'
import { NavLink, useNavigate } from 'react-router-dom'
import instance from '../../shared/constant'
import './style.css'
function Login () {
  const [data, setData] = useState([])
  const getData = () => {
    instance.get('data').then((res) => setData(res.data))
  }
  useEffect(() => {
    getData()
  }, [])

  const navigate = useNavigate()
  const [userRegistration, setUserRegistration] = useState({
    email: '',
    password: ''
  })

  const handleChange = (e) => {
    const { name, value } = e.target
    setUserRegistration({ ...userRegistration, [name]: value })
  }
  const handleSubmit = (e) => {
    const findEmail = data.find(
      (item) => item.email === userRegistration.email
    )
    if (findEmail.password === userRegistration.password) {
      sessionStorage.setItem('password', userRegistration.password)
      navigate('/navbar')
    } else {
      alert('email or Password are incorrect')
    }
    e.preventDefault()
  }

  return (
    <>
      <div className="form-container">
        <h1>Login</h1>
        <form onSubmit={(e) => handleSubmit(e)} className="form">
          <div className="row">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              name="email"
              autoComplete="off"
              placeholder="email@example.com"
              onChange={(e) => handleChange(e)}
              required={true}
            />
          </div>
          <div className="row">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              name="password"
              onChange={(e) => handleChange(e)}
              required={true}
            />
          </div>
          <button type="submit" className="login-button">
            Login
          </button>
          <NavLink to="/singUp">Go Back To Registration</NavLink>
        </form>
      </div>
    </>
  )
}

export default Login
