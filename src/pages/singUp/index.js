import React, { useState } from 'react'
import { NavLink, useLocation, useNavigate } from 'react-router-dom'
import instance from '../../shared/constant'
import './style.css'
const SingUp = () => {
  const navigate = useNavigate()
  const { state } = useLocation()
  console.log('state', state)
  // const id = state.item.id
  // const [userRegistration, setUserRegistration] = useState({
  //   firstName: '',
  //   lastName: '',
  //   email: '',
  //   jobTitle: '',
  //   gender: '',
  //   profilePic: '',
  //   country: '',
  //   address: '',
  //   bio: '',
  //   mobile: '',
  //   password: '',
  //   status: ''
  // })
  const [firstName, setFirstName] = useState()
  const [lastName, setLastName] = useState()
  const [email, setEmail] = useState()
  const [jobTitle, setJobTitle] = useState()
  const [gender, setGender] = useState()
  const [profilePic, setProfilePic] = useState()
  const [country, setCountry] = useState()
  const [address, setAddress] = useState()
  const [bio, setBio] = useState()
  const [mobile, setMobile] = useState()
  const [password, setPassword] = useState()
  const [status, setStatus] = useState()

  // const [records, setRecords] = useState([])

  // const handleChange = (e) => {
  //   const { name, value } = e.target
  //   setUserRegistration({ ...userRegistration, [name]: value })
  // }

  // const handleSubmit = (e) => {
  //   setRecords(records => [...records, userRegistration])
  //   console.log('records', records)
  //   localStorage.setItem('formValue', JSON.stringify([...records, userRegistration]))
  //   e.preventDefault()
  //   navigate('/')
  // }

  const handleSubmit = (e) => {
    localStorage.setItem('formValue', JSON.stringify(email, password))

    instance
      .post('data', {
        firstName,
        lastName,
        email,
        jobTitle,
        gender,
        profilePic,
        country,
        address,
        bio,
        mobile,
        password,
        status
      })
      .then((res) => console.log('res.data :>> ', res.data))
      .then(() => navigate('/'))
    e.preventDefault()
  }

  // useEffect(() => {
  //   const items = JSON.parse(localStorage.getItem('formValue'))
  //   console.log('getData', items)
  //   if (items) {
  //     setRecords(items)
  //   }
  // }, [])
  return (
    <>
      <div className="Form-body">
        <div className="container-form">
          <div className="title">
            <h1>Registration Form</h1>
          </div>
          <form className="sign-form">
            <div className="name">
              <div className="firstName">
                <label htmlFor="firstName">firstName</label>
                <input
                  type="text"
                  name="firstName"
                  id="name"
                  value={firstName}
                  className="input"
                  onChange={(e) => setFirstName(e.target.value)}
                  required={true}
                />
              </div>
              <div className="lastName">
                <label htmlFor="lastName">LastName</label>
                <input
                  type="text"
                  name="lastName"
                  id="lastName"
                  value={lastName}
                  className="input"
                  onChange={(e) => setLastName(e.target.value)}
                  required={true}
                />
              </div>
            </div>
            <div className="name">
              <div className="Email">
                <label htmlFor="Email">Email</label>
                <input
                  type="email"
                  value={email}
                  name="email"
                  className="email"
                  onChange={(e) => setEmail(e.target.value)}
                  required={true}
                />
              </div>
              <div className="JobTitle">
                <label htmlFor="JobTitle">JobTitle</label>
                <input
                  type="input"
                  name="jobTitle"
                  value={jobTitle}
                  className="input"
                  onChange={(e) => setJobTitle(e.target.value)}
                />
              </div>
            </div>
            <div className="name">
              <div className="profilePic">
                <label htmlFor="profilePic">ProfilePic</label>
                <input
                  type="file"
                  name="profilePic"
                  value={profilePic}
                  className="profilePic"
                  onChange={(e) => setProfilePic(e.target.value)}
                />
              </div>

              <div className="country">
                <label htmlFor="country">Country</label>
                <input
                  type="input"
                  name="country"
                  value={country}
                  className="input"
                  onChange={(e) => setCountry(e.target.value)}
                />
              </div>
            </div>

            <div className="name">
              <div className="address">
                <label htmlFor="address">address</label>
                <textarea
                  name="address"
                  value={address}
                  className="address"
                  onChange={(e) => setAddress(e.target.value)}
                />
              </div>
              <div className="Bio">
                <label htmlFor="Bio">Bio</label>
                <textarea
                  name="bio"
                  value={bio}
                  className="input"
                  onChange={(e) => setBio(e.target.value)}
                />
              </div>
            </div>

            <div className="name">
              <div className="mobile">
                <label htmlFor="mobile">mobile No</label>
                <input
                  type="number"
                  name="mobile"
                  value={mobile}
                  className="input"
                  onChange={(e) => setMobile(e.target.value)}
                  required={true}
                />
              </div>
              <div className="Password">
                <label htmlFor="Password">password</label>
                <input
                  type="password"
                  name="password"
                  value={password}
                  className="input"
                  onChange={(e) => setPassword(e.target.value)}
                  required={true}
                />
              </div>
            </div>
            <div className="Gender">
              <label htmlFor="Gender">Gender</label>
              <input
                type="radio"
                name="gender"
                value={gender}
                className="gender"
                onChange={(e) => setGender(e.target.value)}
                required={true}
              />{' '}
              Male
              <input
                type="radio"
                name="gender"
                value={gender}
                className="gender"
                onChange={(e) => setGender(e.target.value)}
                required={true}
              />{' '}
              Female
            </div>
            <div className="status">
              <label htmlFor="status">status</label>
              <input
                type="radio"
                name="status"
                value={status}
                className="status"
                onChange={(e) => setStatus(e.target.value)}
              />
              True
              <input
                type="radio"
                value={status}
                name="status"
                className="status"
                onChange={(e) => setStatus(e.target.value)}
              />
              False
            </div>
            <button
              type="submit"
              className="btn btn-primary"
              onClick={(e) => handleSubmit(e)}
            >
              Submit
            </button>
            <div>
              <NavLink to="/">You Have All ready Account Goto Sing in</NavLink>
            </div>
          </form>
        </div>
      </div>
    </>
  )
}
export default SingUp
