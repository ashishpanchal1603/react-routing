import React, { useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import instance from '../../shared/constant'
import './style.css'
const UpdatePage = () => {
  const navigate = useNavigate()
  const { state } = useLocation()
  console.log('state', state)
  const id = state.item.id
  const [firstName, setFirstName] = useState(state.item.firstName)
  const [lastName, setLastName] = useState(state.item.lastName)
  const [email, setEmail] = useState(state.item.email)
  const [jobTitle, setJobTitle] = useState(state.item.jobTitle)
  const [address, setAddress] = useState(state.item.address)
  const [bio, setBio] = useState(state.item.bio)
  const [mobile, setMobile] = useState(state.item.mobile)
  const [status, setStatus] = useState(state.item.status)

  const handleSubmit = (e) => {
    instance
      .put(`data/${id}`, {
        firstName,
        lastName,
        email,
        jobTitle,
        address,
        bio,
        mobile,
        status
      })
      .then((res) => console.log('res.data :>> ', res.data))
      .then(() => navigate('/navbar/user'))
    e.preventDefault()
  }

  return (
    <>
      <div className="Form-body">
        <div className="container-form">
          <div className="title">
            <h1>Registration Form</h1>
          </div>
          <form className="sign-form">
            <div className="name">
              <div className="firstName">
                <label htmlFor="firstName">firstName</label>
                <input
                  type="text"
                  name="firstName"
                  id="name"
                  value={firstName}
                  className="input"
                  onChange={(e) => setFirstName(e.target.value)}
                  required={true}
                />
              </div>
              <div className="lastName">
                <label htmlFor="lastName">LastName</label>
                <input
                  type="text"
                  name="lastName"
                  id="lastName"
                  value={lastName}
                  className="input"
                  onChange={(e) => setLastName(e.target.value)}
                  required={true}
                />
              </div>
            </div>
            <div className="name">
              <div className="Email">
                <label htmlFor="Email">Email</label>
                <input
                  type="email"
                  value={email}
                  name="email"
                  className="email"
                  onChange={(e) => setEmail(e.target.value)}
                  required={true}
                />
              </div>
              <div className="JobTitle">
                <label htmlFor="JobTitle">JobTitle</label>
                <input
                  type="input"
                  name="jobTitle"
                  value={jobTitle}
                  className="input"
                  onChange={(e) => setJobTitle(e.target.value)}
                />
              </div>
            </div>

            <div className="name">
              <div className="address">
                <label htmlFor="address">address</label>
                <textarea
                  name="address"
                  value={address}
                  className="address"
                  onChange={(e) => setAddress(e.target.value)}
                />
              </div>
              <div className="Bio">
                <label htmlFor="Bio">Bio</label>
                <textarea
                  name="bio"
                  value={bio}
                  className="input"
                  onChange={(e) => setBio(e.target.value)}
                />
              </div>
            </div>

            <div className="name">
              <div className="mobile">
                <label htmlFor="mobile">mobile No</label>
                <input
                  type="number"
                  name="mobile"
                  value={mobile}
                  className="input"
                  onChange={(e) => setMobile(e.target.value)}
                />
              </div>
            </div>

            <div className="status">
              <label htmlFor="status">status</label>
              <input
                type="input"
                name="status"
                value={status}
                className="status"
                onChange={(e) => setStatus(e.target.value)}
              />{' '}
              {/* True */}
              {/* <input
                type="radio"
                value={status}
                name="status"
                className="status"
                onChange={(e) => setStatus(e.target.value)}
              />{' '}
              False */}
            </div>
            <button
              type="submit"
              className="btn btn-primary"
              onClick={(e) => handleSubmit(e)}
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    </>
  )
}
export default UpdatePage
