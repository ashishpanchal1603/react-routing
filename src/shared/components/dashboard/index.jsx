import React, { useEffect, useState } from 'react'
import instance from '../../constant'
import './style.css'

function DashBoard () {
  const [data, setData] = useState([])
  const getData = () => {
    instance.get('data').then((res) => setData(res.data))
  }
  useEffect(() => {
    getData()
  }, [])

  const totalUser = data.length
  const male = data.filter((item) => item.gender === 'Male')
  const maleUser = male.length
  const female = data.filter((item) => item.gender === 'Female')
  const femaleUser = female.length
  console.log('male :>> ', femaleUser)
  return (
    <>
      <div className="user-main-details">
        <h1>Dashboard</h1>
        <div>
          total number of user <span>{totalUser}</span>{' '}
        </div>
        <div>
          total Male User <span>{maleUser}</span>{' '}
        </div>
        <div>
          total feMale User <span>{femaleUser}</span>{' '}
        </div>
      </div>
    </>
  )
}

export default DashBoard
