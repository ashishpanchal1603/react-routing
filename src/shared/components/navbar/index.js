import React from 'react'
import { NavLink, Outlet } from 'react-router-dom'
import './style.css'

function Navbar () {
  return (
    <>
      <nav className="navbar">
        <div className="navbar-container container">
            <input type="checkbox" name="" id=""/>
            <div className="hamburger-lines">
                <span className="line line1"></span>
                <span className="line line2"></span>
                <span className="line line3"></span>
            </div>
            <ul className="menu-items">
                <li><NavLink to="">Home</NavLink></li>
                <li><NavLink to="/navbar/user">User</NavLink></li>
            </ul>
        </div>
    </nav>
        <Outlet/>
    </>
  )
}

export default Navbar
