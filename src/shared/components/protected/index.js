import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import PropTypes from 'prop-types'
function Protected (props) {
  const { Component } = props
  const navigate = useNavigate()
  useEffect(() => {
    const password = sessionStorage.getItem('password')
    console.log('password', password)
    if (!password) {
      navigate('/')
    }
  })
  return (
    <div>
      <Component />
    </div>
  )
}

export default Protected
Protected.propTypes = {
  Component: PropTypes.func.isRequired
}
